#!/usr/bin/env bash

branch=$(git branch --show-current)

git checkout main && \
git pull origin main && \
git branch -D "$branch"
