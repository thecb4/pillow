#!/usr/bin/env bash

echo "Testing"
swift test --skip-build --parallel --enable-code-coverage --xunit-output .build/debug/test-results.xml && \
swift test --show-codecov-path