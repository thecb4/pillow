#!/usr/bin/env bash

echo "📝 Formatting"
swift format --configuration .format.json -m format --recursive --in-place Package.swift Sources Tests

echo "🧹 Linting"
swift format --configuration .format.json -m lint --recursive Package.swift Sources Tests